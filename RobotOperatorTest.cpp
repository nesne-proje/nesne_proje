﻿#include<iostream>
#include"RobotOperator.h"
using namespace std;
int main() {
    RobotOperator person1("Memduh", "Aslan", 4389);
    if (person1.checkAccessCode(4389)) {
        cout << "Entered key is same with password" << endl;
    }
    else {
        cout << "Entered key is NOT same with password" << endl;
    }
    person1.print();
    cout << "Password of person1 is: " << person1.decryptCode(person1.encryptCode(4389)) << endl;
    cout << endl;

    RobotOperator person2("Husnu", "Coban", 9876);
    if (person2.checkAccessCode(7691)) {
        cout << "Entered key is same with password" << endl;
    }
    else {
        cout << "Entered key is NOT same with password" << endl;
    }
    person2.print();
    cout << "Password of person2 is: " << person2.decryptCode(person2.encryptCode(9876)) << endl;
    cout << endl;

    return 0;
}