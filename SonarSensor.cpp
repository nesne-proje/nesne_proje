#include "SonarSensor.h"
#include "PioneerRobotAPI.h"
using namespace std;
SonarSensor::SonarSensor(PioneerRobotAPI* robotAPI):RangeSensor()
{
	this->robotAPI= robotAPI;
}
SonarSensor::SonarSensor()
{
}
//*******************************************************************************//
void SonarSensor::updateSensor(float ranges[])
{
	this->robotAPI->getSonarRange(ranges);
	for (int i = 0; i < 16; i++)
	{
		this->ranges[i] = ranges[i];
	}
}
//*******************************************************************************//
float SonarSensor::getRange(int index)
{
	return this->ranges[index];
}
//*******************************************************************************//
float SonarSensor::getMax(int& index)
{
	if (index >= 16)throw exception("SonarSensor::getMax-Index out of range");
	float max;
	max = ranges[0];
	index = 0;
	for (int i = 1; i < 16; i++)
	{
		if (ranges[i]>max)
		{
			max = ranges[i];
			index = i;
		}
	}
	return max;
}
//*******************************************************************************//
float SonarSensor::getMin(int& index)
{
	if (index >= 16)throw exception("SonarSensor::getMin-Index out of range");
	float min;
	min = ranges[0];
	for (int i = 1; i < 16; i++)
	{
		if (ranges[i] < min)
		{
			min = ranges[i];
			index = i;
		}
	}
	return min;
}
//*******************************************************************************//
float SonarSensor::getAngle(int index)
{
		if (index >= 16)throw std::exception("SonarSensor::getAngle-Index out of range");
		float rangeAngles[16] = { 90,50,30,10,-10,-30,-50,-90,-90,-50,-30,-10,10,30,50,90 };
		cout << "Range[" << index << "] Angle: ";
		return rangeAngles[index];
}
//*******************************************************************************//
float SonarSensor::operator[](int i) 
{
	if (i >= 16)throw exception("SonarSensor::operator[]-Index out of range");
	cout << "Range[" << i << "]:";
	return this->ranges[i];
}
//*******************************************************************************//
SonarSensor::~SonarSensor()
{
}
float SonarSensor::getClosestRange(float startAngle, float endAngle, float& angle)
{
	if (startAngle < 0 || startAngle>180 || endAngle < 0 || endAngle>180)throw exception("Index is invalid number!\n");
	float minC = this->ranges[(int)startAngle];
	for (int i = (int)startAngle + 1; i < endAngle; i++)
	{
		if (this->ranges[i] < minC)
		{
			minC = this->ranges[i];
			angle = i;
		}
	}
	cout << "Closest Range: ";
	return minC;
}