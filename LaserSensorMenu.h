/**
 * @file LaserSensorMenu.h
 * @Author Omer Faruk LALE
 * @date January, 2021
 * @brief Menu for Laser Sensor.
 */
#include "LaserSensor.h"
class LaserSensorMenu {
public:
    /**
    * @brief: This functions runs the menu which is related with Laser Sensor.
    * @param: PioneerRobotAPI
    */
	void Menu(PioneerRobotAPI*);
};