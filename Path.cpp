#include "Path.h"

Path::Path()
{
	this->head = this->tail = NULL;
	this->number = 0;
}

Path::~Path()
{
	while (this->head != NULL) {
		Node* node = this->head;
		this->head = this->head->next;
		delete node;
	}
	this->number = 0;
	this->tail = NULL;
}

void Path::addPose(Pose pose)
{
	Node* node = new Node(pose);

	if (this->head == NULL) {
		this->head = this->tail = node;
	}
	else {
		this->tail->next = node;
		this->tail = node;
	}
	this->number++;
}

void Path::print()
{
	Node* p = head;
	while (p != NULL) {
		cout << p->pose.getX() << ", " << p->pose.getY() << ", " << p->pose.getTh() << endl;
		p = p->next;
	}
}

Pose Path::operator[](int index)
{
	if (index >= this->number) {
		throw exception("Index is greater than number of item!\n");
	}
	Node* p = head;
	for (int i = 0; i < index; i++) {
		p = p->next;
	}
	return p->pose;
}

Pose Path::getPose(int index)
{
	if (index >= this->number) {
		throw exception("Index is greater than number of item!\n");
	}
	Node* p = head;
	for (int i = 0; i < index; i++) {
		p = p->next;
	}
	return p->pose;
}

bool Path::removePose(int index)
{
	if (index >= this->number) {
		throw exception("Index is greater than number of item!\n");
	}
	Node* q = NULL;
	Node* p = head;
	for (int i = 0; i < index; i++) {
		q = p;
		p = p->next;
	}
	//                q  p
//end-while		   _  1, 2, 3,  ,5 , 6, 7	//x=4

	if (p == NULL) return false;  // hi� eleman silemedi�i durum

	if (q == NULL) {
		head = p->next;
		if (this->head == NULL) this->tail = NULL;
	}
	else if (p != tail) {
		q->next = p->next;
	}
	else {
		this->tail = q;
		this->tail->next = NULL;
	}
	delete p;
	return true;	// verilen eleman� sildi�i durum
}

bool Path::insertPose(int index, Pose pose) {
	if (index >= this->number) {
		throw exception("Index is greater than number of item!\n");
	}
	Node* node = new Node(pose);
	Node* q = NULL;
	Node* p = head;
	for (int i = 0; i < index; i++) {
		q = p;
		p = p->next;
	}
	if (q == NULL) {
		node->next = head;
		head = node;
		this->number++;
		return true;
	}
	else if (p == tail) {
		tail->next = node;
		tail = node;
		this->number++;
		return true;
	}
	else {
		p = p->next;
		q = q->next;
		node->next = q->next;
		q->next = node;
		this->number++;
		return true;
	}
	return false;
}
ostream& operator<<(ostream& out, const Path& p)
{
	Node* q = p.head;
	while (q != NULL) {
		out << q->pose.getX() << ", " << q->pose.getY() << ", " << q->pose.getTh() << endl;
		q = q->next;
	}
	return out;
}

istream& operator>>(istream& in, Path& p) {
	float x, y, th;
	in >> x >> y >> th;
	Pose pose(x, y, th);
	p.addPose(pose);
	return in;
}