/**
 * @file Pose.h
 * @Author Ezgi Özdikyar - 152120171104
 * @date January, 2021
 * @brief This class has the task of managing the robot's information.
 * 
 * This class handles the robot's information, updating and managing it. 
 * Inside the class there are 3 variables to hold the robot's x coordinate, 
 * y coordinate and angle value.
 */
#pragma once
#include<math.h>
#include<iostream>
using namespace std;
class Pose
{
private:
	float x;
	float y;
	float th;	
public:
	/**
	* @brief: Parameterized constructor. 
	* @param: float x - x coordinate of robot 
	* @param: float y - y coordinate of robot 
	* @param: float th - angle theta
	*/
	Pose(float, float, float);
	/**
	* @brief: Default constructor.
	*/
	Pose();
	/**
	* @brief: This functions returns x coordinate of robot.
	* @return: float x - x coordinate
	*/
	float getX()const;
	/**
	* @brief: This functions sets x coordinate of robot.
	* @param: float x - x coordinate
	*/
	void setX(float);
	/**
	* @brief: This functions returns y coordinate of robot.
	* @return: float y - y coordinate
	*/
	float getY()const;
	/**
	* @brief: This functions sets y coordinate of robot.
	* @param: float y - y coordinate
	*/
	void setY(float);
	/**
	* @brief: This functions returns angle theta.
	* @return: float th - angle theta
	*/
	float getTh()const;
	/**
	* @brief: This functions sets angle of robot.
	* @param: float th - angle theta
	*/
	void setTh(float);
	/**
	* @brief: This functions controls if two Pose class objects have the same values or not.
	* @param: Pose& other - Pose class object
	* @return: bool - true or false
	*/
	bool operator==(const Pose&);
	/**
	* @brief: This functions sums up Pose class objects's values and assings it.
	* @param: Pose& other - Pose class object
	* @return: Pose& - Pose class object
	*/
	Pose& operator+=(const Pose&);
	/**
	* @brief: This functions substracts Pose class objects's values and assings it.
	* @param: Pose& other - Pose class object
	* @return: Pose& - Pose class object
	*/
	Pose& operator-=(const Pose&);
	/**
	* @brief: This functions sums up Pose class objects's values and assings it to new Pose class object.
	* @param: Pose& other - Pose class object
	* @return: Pose& sum - Pose class object
	*/
	Pose operator+(const Pose&);
	/**
	* @brief: This functions substracts Pose class objects's values and assings it to new Pose class object.
	* @param: Pose& other - Pose class object
	* @return: Pose& substraction - Pose class object
	*/
	Pose operator-(const Pose&);
	/**
	* @brief: This functions controls if this class's object's values are less then the other object's values.
	* @param: Pose& other - Pose class object
	* @return: bool - true or false
	*/
	bool operator<(const Pose&);
	/**
	* @brief: This functions returns the distance between two position.
	* @param: Pose& other - Pose class object
	* @return: float - distance
	*/
	float findDistanceTo(Pose);
	/**
	* @brief: This functions returns the angle between two position.
	* @param: Pose& other - Pose class object
	* @return: float - angle
	*/
	float findAngelTo(Pose);
	/**
	* @brief: This functions sets the values of Pose class.
	* @param: float x - x coordinate of robot
	* @param: float y - y coordinate of robot
	* @param: float th - angle theta
	*/
	void setPose(float,float,float);
	/**
	* @brief: This functions gets the Pose class object.
	* @return: Pose& - Pose class object
	*/
	Pose& getPose();
	/**
	* @brief: This functions prints all values.
	*/
	void print()const;
};
