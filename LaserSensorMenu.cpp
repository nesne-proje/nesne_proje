#include "LaserSensorMenu.h"
using namespace std;
void LaserSensorMenu::Menu(PioneerRobotAPI* robot)
{
	int index = 0;
	float laserArray[1000];
	LaserSensor* lRobot;
	lRobot = new LaserSensor(robot);
	string choice;
	do
	{
		cout << endl << "Laser Sensor Menu" << endl << "1. Range" << endl << "2. Maximum" <<
			endl << "3. Minimum" << endl << "4. Angle" <<endl<< "5. Closest Range"<<
			endl << "6. Quit" << endl;
		cout << "Choose one : ";
		getline(cin, choice);
		if (choice == "1")
		{
			cout << endl << "<Range>" << endl << "Calculating range..." << endl;
			try
			{
				cout << "Enter index: ";
				cin >> index;
				if (index >= 1000)throw exception("You must enter index between 0-181");
				lRobot->updateSensor(laserArray);
				cout << lRobot->getRange(index);
				cin.ignore();
			}
			catch (const std::exception& e)
			{
				cout << e.what() << endl;
			}
		}
		else if (choice == "2")
		{
			cout << endl << "<Maximum>" << endl << "Calculating maximum..." << endl;
			try
			{
				cout << "Enter index: ";
				cin >> index;
				if (index >= 1000)throw exception("You must enter index between 0-181");
				cout << lRobot->getMax(index);
				cin.ignore();
			}
			catch (const std::exception& e)
			{
				cout << e.what() << endl;
			}
		}
		else if (choice == "3")
		{
			cout << endl << "<Minimum>" << endl << "Calculating minimum..." << endl;
			try
			{
				cout << "Enter index: ";
				cin >> index;
				if (index >= 1000)throw exception("You must enter index between 0-181");
				cout << lRobot->getMin(index);
				cin.ignore();
			}
			catch (const std::exception& e)
			{
				cout << e.what() << endl;
			}
		}
		else if (choice == "4")
		{
			cout << endl << "<Angle>" << endl << "Calculating angle..." << endl;
			try
			{
				cout << "Enter index: ";
				cin >> index;
				if (index >= 1000)throw exception("You must enter index between 0-181");
				cout << lRobot->getAngle(index);
				cin.ignore();
			}
			catch (const std::exception& e)
			{
				cout << e.what() << endl;
			}
		}
		else if (choice == "5")
		{
			float startAngle, endAngle, angle;
			cout << endl << "<Closest Range>" << endl << "Calculating closest range..." << endl;
			try
			{
				cout << "Enter startAngle endAngle and angle:";
				cin >> startAngle >> endAngle >> angle;
				if (startAngle < 0 || startAngle>1000 || endAngle < 0 || endAngle>180)throw exception("Index is invalid number!\n");
				cout << lRobot->getClosestRange(startAngle,endAngle,angle);
				cin.ignore();
			}
			catch (const std::exception& e)
			{
				cout << e.what() << endl;
			}
		}
		else if (choice == "6")
			break;
		else
			cout << endl << choice << " is not an option!" << endl << endl;
		cout << endl;
	} while (1);
}