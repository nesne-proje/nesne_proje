/**
 * @file RobotControl.h
 * @Author  Mine Cakir
 * @date January, 2021
 * @brief This class manages and updates positions of robot.
 */
#pragma once
#include "PioneerRobotAPI.h"
#include "Pose.h"
#include "Record.h"
#include "Path.h"
#include"RobotOperator.h"
#include"RobotInterface.h"
#include"PionerRobotInterface.h"
class RobotControl
{
private:
	Path* path;
	Record* record;
	RobotOperator* robotOperator;
	RobotInterface* robotInterface;
	RangeSensor* rangeSensor;
	int state;
	bool isCodeTrue;
	static int numberOfPaths;
public:
	/**
	* @brief: Constructor to set initialize values.
	* @param: PioneerInterface object.
	*/
	RobotControl();
	/**
	* @brief: Constructor2 to set initialize values.
	* @param: int state value, float x y and th values.
	*/
	RobotControl(int,float,float,float);
	/**
	* @brief: Destructor.
	*/
	~RobotControl();
	/**
	* @brief: This function sets position.
	* @param: Pose object.
	*/
	void setPose(Pose*);
	/**
	* @brief: This function returs position.
	*/
	Pose getPose();
	/**
	* @brief: Moves the robot forward.
	* @param: float speed --> moving speed.
	*/
	void forward(float);
	/**
	* @brief: Moves the robot backward.
	* @param: float speed --> moving speed.
	*/
	void backward(float);
	/**
	* @brief: Rotates the robot to the left.
	*/
	void turnLeft();
	/**
	* @brief: Rotates the robot to the right.
	*/
	void turnRight();
	/**
	* @brief: This function stops the robot from turning.
	*/
	void stopTurn();
	/**
	* @brief: This functions prints position and state values.
	*/
	void print();
	/**
	* @brief: This function stops the moving robot.
	*/
	void stopMove();
	/** 
	* @brief: This function updates position of robot by robotInterface. 
	*/
	void updateRobot();
	/**
	* @brief: This function adds position to path.
	* @return: true - false 
	*/
	bool addToPath();
	/**
	* @brief: This function deletes all positions in the path.
	* @return: true - false
	*/
	bool clearPath();
	/**
	* @brief: This function writes positions of robot to a file.
	* @return: true - false
	*/
	bool recordPathToFile();
	/**
	* @brief: If given code is true then this function allows to access all methods of this class.
	* @param: int code - code entering by user.
	* @return: true - false
	*/
	bool openAccess(int);
	/**
	* @brief: If given code is false then this function does not allow to access all methods of this class.
	* @param: int code - code entering by user.
	* @return: true - false
	*/
	bool closeAccess(int);
	/**
	* @brief: This function sets RobotOperator object.
	* @param: RobotOperator* robotoperator - RobotOperator object
	*/
	void setRobotOperator(RobotOperator*);
	/**
	* @brief: This function sets RobotInterface object.
	* @param: RobotInterface* robotInterface - RobotInterface object
	*/
	void setRobotInterface(RobotInterface*);
};

