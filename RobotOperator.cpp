#include "RobotOperator.h"

RobotOperator::RobotOperator():name(""),surname(""), accessCode(0), accessState(0),enc()
{
}

RobotOperator::RobotOperator(string name, string surname, unsigned int num) :name(name), surname(surname)
{
    accessCode = enc.encrypt(num);
    accessState = false;
}

int RobotOperator::encryptCode(int num)
{
    return enc.encrypt(num);
}

int RobotOperator::decryptCode(int num)
{
    return enc.decrypt(num);
}

bool RobotOperator::checkAccessCode(int num)
{
    if (num == enc.decrypt(accessCode)) {
        accessState = 1;
        return accessState;
    }
    else {
        accessState = 0;
        return accessState;
    }
}

void RobotOperator::print()
{
    cout << "Name: " << name << endl;
    cout << "Surname: " << surname << endl;
    cout << "Access State: " << accessState << endl;
}

void RobotOperator::operator=(RobotOperator& r)
{
    this->accessCode = r.accessCode;
    this->accessState = r.accessState;
    this->name = r.name;
    this->surname = r.surname;
    this->enc = r.enc;
}
