/**
 * @Author Ezgi �zdikyar - 152120171104
 * @date January, 2021
 */
#if 1
#include "RobotControlMenu.h"
#include "SonarSensorMenu.h"
#include "LaserSensorMenu.h"
#include "RobotOperator.h"
int main()
{
	PioneerRobotAPI* robot;
	robot = new PioneerRobotAPI;
	RobotControlMenu motionMenu;
	SonarSensorMenu sonarSensorMenu;
	LaserSensorMenu laserSensorMenu;
	const int robotPassword = 4389;
	string choice;
	string name, surname;
	int password;
	cout << "Enter name:";
	cin >> name;
	cout << "Enter surname:";
	cin >> surname;
	cout << "Enter password:(Correct password 4389 ;) )";
	cin >> password;
	RobotOperator person1(name, surname, password);
	cin.ignore();
	cout << endl;
	if (person1.checkAccessCode(4389)) {
		cout << "Entered key is same with password. Menu is starting...." << endl;
		do {

			cout << "Main Menu" << endl;
			cout << "1. Connection" << endl << "2. Motion" <<
				endl << "3. Sonar Sensor" << endl << "4. Laser Sensor" << endl << "5. Quit" << endl;
			cout << "Choose one : ";
			getline(cin, choice);
			if (choice == "1")
			{
				cout << endl << "Connection Menu" << endl << "1. Connect Robot" << endl <<
					"2. Disconnect Robot" << endl << "3. Back" << endl;
				cout << "Choose one : ";
				getline(cin, choice);
				if (choice == "1")
				{
					robot->connect();
				}
				else if (choice == "2")
				{
					robot->disconnect();
				}
				else if (choice == "3")
					break;
				else
					cout << endl << choice << " is not an option!" << endl << endl;
			}
			else if (choice == "2")
			{
				motionMenu.Menu(robot);
			}
			else if (choice == "3")
			{
				sonarSensorMenu.Menu(robot);
			}
			else if (choice == "4")
			{
				laserSensorMenu.Menu(robot);
			}
			else if (choice == "5")
			{
				cout << "Exiting from the menu..." << endl;
				break;
			}
			else
			{
				cout << endl << choice << " is not an option!" << endl << endl;
			}
			cout << endl;
		} while (1);
	}
	else {
		cout << "Entered key is NOT same with password" << endl;
		exit(0);
	}
}
#endif