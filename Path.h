/**
 * @file Path.h
 * @Author Mesut KIZILAY 
 * @date January, 2021
 * @brief It is the class that allows managing the road plan in the linked list form.
 */
#pragma once
#include"Node.h"
class Path {
	friend ostream& operator<<(ostream&, const Path&);
	friend istream& operator>>(istream&, Path&);
private:
	Node* tail;
	Node* head;
	int number;
public:
	/**
	* @brief: Default constructor.
	*/
	Path();
	/**
	* @brief: Destructor.
	*/
	~Path();
	/**
	* @brief: This functions adds the given position to the end of the list.
	* @param: Pose pose - Pose class object.
	*/
	void addPose(Pose pose);
	/**
	* @brief: This function prints the positions in the list on the screen.
	*/
	void print();
	/**
	* @brief: This functions returns the position given the index.
	* @param: int index - index
	* @return: Pose - Position.
	*/
	Pose operator[](int index);
	/**
	* @brief: This functions returns the position in the given index.
	* @param: int index - index
	* @return: Pose - Position.
	*/
	Pose getPose(int index);
	/**
	* @brief: This functions deletes the position in the given index from the list.
	* @param: int index - index
	* @return: bool - true or false
	*/
	bool removePose(int index);
	/**
	* @brief: This functions adds a position after the given index.
	* @param: int index - index
	* @param: Pose pose - Pose class object.
	* @return: bool - true or false
	*/
	bool insertPose(int index, Pose pose);
};