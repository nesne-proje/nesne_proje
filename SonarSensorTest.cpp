#if 0
/**
*@author: Mine Çakır
*@date: 10.01.2021
*/
#include "SonarSensor.h"
#include "PioneerRobotAPI.h"
PioneerRobotAPI* robot;
SonarSensor* sRobot;
int main()
{
	float sonars[16];
	float min, max;
	robot = new PioneerRobotAPI(); //Created PioneerRobotAPI obj
	sRobot = new SonarSensor(robot); //Created SonarSensor obj
	if (!robot->connect()) {
		std::cout << "Could not connect..." << std::endl;
		return 0;
	}
	try
	{
		sRobot->updateSensor(sonars); //Call updateSensor function and fill in sonars array.
		std::cout << "Sonar ranges are [ ";
		for (int i = 0; i < 16; i++) {
			std::cout << sonars[i] << " ";
		}
		std::cout << "]" << std::endl;
		sRobot->updateSensor(sonars);
		//***************************************************//
		std::cout << "Range[5]-->" << sRobot->getRange(5) << std::endl; // Test getRange() function
		int a = 5;
		min = sRobot->getMin(a); // Calculate min value in ranges
		max = sRobot->getMax(a); // Calculate max value in ranges
		std::cout << "Max value:" << max << " Min value:" << min << std::endl;
		std::cout << "Sonar ranges are [ ";
		for (int i = 0; i < 16; i++) {
			std::cout << sRobot->getRange(i) << " ";
		}
		std::cout << "]" << std::endl;
		std::cout << sRobot->getAngle(5) << std::endl; // Call getAngle() funcion in the given index
		std::cout << sRobot->operator[](5) << std::endl; //Call operator function and display value that returns
		/// 
		/// If index out of range
		/// 
		std::cout << sRobot->getAngle(20) << std::endl;
	}
	catch (const std::exception&e)
	{
		std::cout << e.what();
	}
}
#endif