#include "RobotControlMenu.h"
RobotControlMenu::RobotControlMenu(PioneerRobotAPI* robot)
{
	rI = new PionerRobotInterface(robot);
	cRobot = new RobotControl();
	cRobot->setRobotInterface(rI);
}
void RobotControlMenu::Menu()
{
	string choice;
	float x, y, th;
	do {
		cout << endl << "Motion Menu" << endl << "1. Move Robot" << endl <<
			"2. Safe Move Robot" << endl << "3. Turn Left" << endl <<
			"4. Turn Right" << endl << "5. Forward" << endl <<
			"6. Backward" << endl << "7. Move Distance" <<
			endl << "8. Stop Turn " << endl << "9. Set Position" << endl <<
			"10. Get Position" << endl << "11. Quit" << endl;
			cout << "Choose one : ";
			getline(cin, choice);
			if (choice == "1")
			{
				cout << endl << "<Move Robot>" << endl << "Robot is moving..." << endl;
				cRobot->forward(1);
			}
			else if (choice == "2")
			{
				cout << endl << "<Safe Move Robot>" << endl << "Robot is moving ..." << endl;
				cRobot->forward(0);
			}
			else if (choice == "3")
			{
				cout << endl << "<Turn Left>" << endl << "Robot is turning left..." << endl;
				cRobot->turnLeft();
			}
			else if (choice == "4")
			{
				cout << endl << "<Turn Right>" << endl << "Robot is turning right..." << endl;
				cRobot->turnRight();
			}
			else if (choice == "5")
			{
				float speed;
				cout << endl << "<Forward>" << endl << "..." << endl;
				cout << "Enter speed: ";
				cin >> speed;
				cRobot->forward(speed);
				cin.ignore();
			}
			else if (choice == "6")
			{
				float speed;
				cout << endl << "<Backward>" << endl << "..." << endl;
				cout << "Enter speed: ";
				cin >> speed;
				cRobot->backward(speed);
				cin.ignore();
			}
			else if (choice == "7")
			{
				cout << endl << "<Move Distance>" << endl << "..." << endl;
				float distance=0;
				int time=0;
				cout << "Enter distance(mm):";
				cin >> distance;
				time = distance;
				Sleep(time);
				cRobot->stopMove();
				cin.ignore();
			}
			else if (choice == "8")
			{
				cout << endl << "<Stop Turn>" << endl << "..." << endl;
				cRobot->stopTurn();
			}
			else if (choice == "9")
			{
				cout << endl << "<Set Position>" << endl << "Setting position of robot..." << endl;
				cout << "Enter x y and angle:";
				cin >> x >> y >> th;
				Pose position(x, y, th);
				cRobot->setPose(&position);
				cin.ignore();
			}
			else if (choice == "10")
			{
				cout << endl << "<Get Position>" << endl << "Getting position of robot..." << endl;
				cRobot->print();
			}
			else if (choice == "11")
				break;
			else
				cout << endl << choice << " is not an option!" << endl << endl;
			cout << endl;
	} while (1);
}

RobotControl* RobotControlMenu::getRobotControl()
{
	return cRobot;
}

