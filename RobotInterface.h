/**
* @file: RobotInterface.h
*@author: Ezgi Ozdikyar
*@date: January, 2021
* @brief: Interface of robot.
*/
#pragma once
#include "PioneerRobotAPI.h"
#include "Pose.h"
#include "Record.h"
#include"RangeSensor.h"
class RobotInterface
{
protected:
	Pose* position;
	RangeSensor* range;
	int state;
public:
	
	RobotInterface();

	virtual ~RobotInterface();
	/**
	* @brief: This function sets position.
	* @param: Pose object.
	*/
	virtual void setPose(Pose*) = 0;
	/**
	* @brief: This function returs position.
	*/
	virtual Pose getPose()=0;
	/**
	* @brief: Moves the robot forward.
	* @param: float speed --> moving speed.
	*/
	virtual void forward(float)=0;
	/**
	* @brief: Moves the robot backward.
	* @param: float speed --> moving speed.
	*/
	virtual void backward(float)=0;
	/**
	* @brief: Rotates the robot to the left.
	*/
	virtual void turnLeft()=0;
	/**
	* @brief: Rotates the robot to the right.
	*/
	virtual void turnRight()=0;
	/**
	* @brief: This function stops the robot from turning.
	*/
	virtual void stopTurn()=0;
	/**
	* @brief: This functions prints position and state values.
	*/
	virtual void print()=0;
	/**
	* @brief: This function stops the moving robot.
	*/
	virtual void stopMove()=0;
	/** This function is automatically called every one seconds. 
	*/
	virtual void updateRobot()=0;
};

