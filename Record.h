/**
 * @file Record.h
 * @Author Mesut K�z�lay
 * @Author Omer Faruk LALE
 * @date January, 2021
 * @brief It carries out the save to file operations for use in robot applications.
 */
#pragma once
#include<iostream>
#include<string>
#include<fstream>
using namespace std;
class Record {
	/**
	* @brief: This functions writes the data to the file.
	* @param: fstream& out
	* @param: Record& r
	* @return: fstream& out
	*/
	friend Record& operator<<(Record& out, string str);
	/**
	* @brief: This functions reads the data to the file.
	* @param: fstream& in
	* @param: Record& r
	* @return: fstream& in
	*/
	friend Record& operator>>(Record& in, string str);
private:
	string filename;
	ifstream filein;
	ofstream fileout;
public:
	/**
	* @brief: Default constructor.
	*/
	Record();
	/**
	* @brief: This functions gets filename and returns it.
	* @return: string filename - It takes filename.
	*/
	string getFilename();
	/**
	* @brief: This functions opens a file for printing and reading.
	* @return: bool - true or false
	*/
	bool openFile();
	/**
	* @brief: This functions closes a file for printing and reading.
	* @return: bool - true or false
	*/
	bool closeFile();
	/**
	* @brief: This functions gets the name of the file to be printed and read.
	* @param: string name - File name.
	*/
	void setFileName(string name);
	/**
	* @brief: This functions reads one line of data from the file.
	* @return: string line - String read from file.
	*/
	string readLine();
	/**
	* @brief: This functions writes one line of data to the file.
	* @param: string str - String to write in file.
	* @return: bool - true or false
	*/
	bool writeLine(string str);
};