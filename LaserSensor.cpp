#include "LaserSensor.h"
using namespace std;

LaserSensor::LaserSensor():RangeSensor()
{
	for (int i = 0; i < 1000; i++)
		this->ranges[i] = 0;
	this->robotAPI = NULL;
}

LaserSensor::LaserSensor(PioneerRobotAPI* robotAPI):RangeSensor()
{
	for (int i = 0; i < 1000; i++)
	{
		this->ranges[i] = 0;
	}
	this->robotAPI = robotAPI;
}

LaserSensor::~LaserSensor()
{
	
}

float LaserSensor::getRange(int index) { return this->ranges[index]; }

void LaserSensor::updateSensor(float ranges[]) {
	this->robotAPI->getLaserRange(this->ranges);
	for (int i = 0; i < 1000; i++)
	{
		ranges[i] = this->ranges[i];
	}
}

float LaserSensor::getMax(int& index) {
	float max = ranges[0];
	for (int i = 0; i < 10000; i++) {
		if (ranges[i] >= max) {
			max = ranges[i];
			index = i;
		}
	}
	return max;
}

float LaserSensor::getMin(int& index) {
	float min = ranges[0];
	for (int i = 0; i < 1000; i++) {
		if (ranges[i] <= min) {
			min = ranges[i];
			index = i;
		}
	}
	return min;
}

float LaserSensor::operator[](int i) {
	if (i >= 1000) {
		throw exception("Index is invalid number!\n");
	}
	return this->ranges[i];
};

float LaserSensor::getAngle(int index) { return index-90; }

float LaserSensor::getClosestRange(float startAngle, float endAngle, float& angle)
{
	if(startAngle<0||startAngle>1000||endAngle<0||endAngle>1000)throw exception("Index is invalid number!\n");
	float minC = this->ranges[(int)startAngle];
	for (int i = (int)startAngle + 1; i <endAngle; i++)
	{
		if (this->ranges[i] < minC)
		{
			minC = this->ranges[i];
			angle = i;
		}
	}
	cout << "Closest Range: ";
	return minC;
}

