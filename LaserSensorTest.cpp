#if 0
#include "SonarSensor.h"
#include "PioneerRobotAPI.h"
#include "RobotControl.h"
#include "LaserSensor.h"
#include <iostream>
using namespace std;
PioneerRobotAPI* robot;
LaserSensor* lRobot;
int main()
{
	float laserData[181];
	float min, max;
	robot = new PioneerRobotAPI(); //Created PioneerRobotAPI obj
	lRobot = new LaserSensor(robot); //Created SonarSensor obj
	if (!robot->connect()) {
		cout << "Could not connect..." << endl;
		return 0;
	}
	cout << "Laser ranges are [ ";
	lRobot->updateSensor(laserData);
	for (int i = 0; i < 181; i++) {
		cout << laserData[i] << " ";
	}
	cout << "]" << endl;
	cout<<lRobot->getRange(5)<<endl;
	cout<<lRobot->getRange(5)<<endl;
	cout<<lRobot->getRange(5)<<endl;
	cout<<lRobot->getRange(5)<<endl;

	cout<<lRobot->getRange(5)<<endl;
	cout<<lRobot->getRange(5)<<endl;
	int a = 5;
	cout << "Max value:" << lRobot->getMax(a)<<endl;
	cout << "Min value:" << lRobot->getMin(a)<<endl;
	cout << lRobot->operator[](8)<<endl;
	cout <<"Range[8] Angle:"<< lRobot->getAngle(8)<<endl;
	float b, angleC;
	a = 5, b = 15, angleC = 50;
	cout << lRobot->getClosestRange(a, b, angleC)<<endl;
	
	robot->disconnect();
}
#endif