#include "Pose.h"

Pose::Pose(float x, float y, float th) :x(x), y(y), th(th)
{
}

Pose::Pose()
{
    x = y = th = 0.0;
}

float Pose::getX()const
{
    return x;
}

void Pose::setX(float x)
{
    this->x = x;
}

float Pose::getY()const
{
    return y;
}

void Pose::setY(float y)
{
    this->y = y;
}

float Pose::getTh()const
{
    return th;
}

void Pose::setTh(float th)
{
    this->th = th;
}

bool Pose::operator==(const Pose& other)
{
    if (x == other.x && y == other.y && th == other.th)
        return true;
    return false;
}

Pose& Pose::operator+=(const Pose& other)
{
    x += other.x;
    y += other.y;
    th += other.th;
    return *this;
}

Pose& Pose::operator-=(const Pose& other)
{
    x -= other.x;
    y -= other.y;
    th -= other.th;
    return *this;
}

Pose Pose::operator+(const Pose& other)
{
    Pose sum;
    sum.setX(x + other.x);
    sum.setY(y + other.y);
    sum.setTh(th + other.th);
    return sum;
}

Pose Pose::operator-(const Pose& other)
{
    Pose subtraction;
    subtraction.setX(x - other.x);
    subtraction.setY(y - other.y);
    subtraction.setTh(th - other.th);
    return subtraction;
}

bool Pose::operator<(const Pose& other)
{
    if (x < other.x && y < other.y && th < other.th)
        return true;
    return false;
}

float Pose::findDistanceTo(Pose other)
{
    float result;
    result =abs(sqrt(pow(x - other.x, 2) + pow(y - other.y, 2)));
    return result;
}

float Pose::findAngelTo(Pose pose)
{
    float result;
    result = (pose.x * pose.y + (x * y))
        / (sqrt(pow(pose.x,2) + pow(pose.y,2)) * sqrt(pow(x,2) + pow(y,2)));
    result = acos(result) * 180.0 / 3.14159265;
    return result;
}

void Pose::setPose(float _x, float _y, float _th)
{
    x = _x;
    y = _y;
    th = _th;
}

Pose& Pose::getPose()
{
    return *this;
}
void Pose::print()const
{
    cout << "x: " << x << " y: " << y << " theta: " << th << endl;
}
