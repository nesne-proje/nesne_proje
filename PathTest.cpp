#include<iostream>
#include"Path.h"
#include"Pose.h"
using namespace std;
int main() {
    try {
        Pose pose1(1.6, 2.3, 3.05);
        Pose pose2(3.2, 4.6, 6.1);
        Pose pose3(4.8, 6.9, 9.15);
        Pose pose4(8.0, 11.5, 15.25);
        Path path;
        cout << "Adding poses...\n";
        path.addPose(pose1);
        path.addPose(pose2);
        path.addPose(pose3);
        path.addPose(pose4);
        path.print();
        cout << "\nEnter location information Pose(x,y,th)\n";
        cin >> path; 
        cout << "\nPrinting poses...\n";
        cout << path << endl;

        cout <<"path[3].getX(): "<< path[3].getX() << endl;
        cout <<"path[3].getY(): "<< path[3].getY() << endl;
        cout <<"path[3].getTh(): "<< path[3].getTh() << endl;

        cout <<"path.getPose(2).getX(): "<< path.getPose(2).getX() << endl;
        cout <<"path.getPose(0).getTh(): "<< path.getPose(0).getTh() << endl;
        cout << "\nRemoving the third pose...\n";
        path.removePose(3);
        cout << endl;
        path.print();

        Pose p5(6.4, 9.2, 12.2);
        cout << "\nInserting the p5 into third index...\n";
        path.insertPose(2, p5);
        cout << endl;
        path.print();

        cout << "\ngetPose(10)\n";
        path.getPose(10);   // Exception because of 10 items dont exist in the list.
    }
    catch (exception const& ex) {
        cerr << "Exception: " << ex.what() << endl;
    }
    return 0;
}