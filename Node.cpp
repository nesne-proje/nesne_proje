#include "Node.h"

Node::Node():pose(0,0,0)
{
	next = NULL;
}

Node::Node(float x, float y, float th):pose(x,y,th)
{
	next = NULL;
}

Node::Node(Pose p) : pose(p)
{
	this->next = NULL;
}
