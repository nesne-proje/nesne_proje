/**
 * @file RobotControlMenu.h
 * @Author Mine Cakir
 * @date January, 2021
 * @brief This class displays motion menu.
 */
#pragma once
#include "RobotControl.h"
#include"PionerRobotInterface.h"
class RobotControlMenu
{
	RobotControl* cRobot;
	PionerRobotInterface* rI;
public:
	/**
	* @brief: Constructor for RobotControlMenu
	* @param: PioneerRobotAPI* robot - PioneerRobotAPI object
	*/
	RobotControlMenu(PioneerRobotAPI*);
	/**
	* @brief: This function displays Robot Control menu.
	*/
	void Menu();
	/**
	* @brief: This function returns RobotControl object.
	* @return: RobotControl* cRobot - RobotControl object
	*/
	RobotControl* getRobotControl();
};