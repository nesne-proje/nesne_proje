#include "Record.h"

Record::Record() { filename = ""; }

string Record::getFilename() { return filename; }

bool Record::openFile()
{
	filein.open(filename);
	fileout.open(filename,ios::out);
	if (!filein && !fileout) {
		cout << "File that you want to open does not exist!" << endl;
		return false;
	}
	return true;
}

bool Record::closeFile()
{
	if (!filein.is_open()&& !fileout.is_open()) {
		cout << "Any file did not open!" << endl;
		return false;
	}
	filein.close();
	fileout.close();
	return true;
}

void Record::setFileName(string name) { this->filename = name; }

string Record::readLine()
{
	string line;
	getline(filein, line);
	return line;
}

bool Record::writeLine(string str)
{
	if (str != "") {
		fileout << str;
	return true;
	}
	else 
		return false;
}

Record& operator<<(Record& record, string str) 
{
	record.writeLine(str);
	return record;
}

Record& operator>>(Record& record, string str)
{
	str = record.readLine();
	return record;
}