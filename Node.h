/**
 * @file Node.h
 * @Author Ezgi �zdikyar - 152120171104
 * @date January, 2021
 * @brief This class creates nodes for linked list format.
 */
#pragma once
#include"Pose.h"
class Node
{
public:
	Node* next;
	Pose pose;
	/**
	* @brief: Default constructor.
	*/
	Node();
	/**
	* @brief: Parameterized constructor.
	* @param: float x - x coordinate
	* @param: float y - y coordinate
	* @param: float th - angle theta
	*/
	Node(float, float, float);
	/**
	* @brief: Parameterized constructor.
	* @param: Pose p - Pose class object
	*/
	Node(Pose p);
};

