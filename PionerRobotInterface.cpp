#include "PionerRobotInterface.h"
PionerRobotInterface::PionerRobotInterface():RobotInterface()
{
	state = 0;
	this->robotAPI->setRobot(robotAPI);
	this->position = new Pose();
}
PionerRobotInterface::PionerRobotInterface(PioneerRobotAPI* robotAPI):RobotInterface()
{
	this->robotAPI = robotAPI;
	state = 0;
	this->robotAPI->setRobot(robotAPI);
	this->position = new Pose();
}
//*******************************************************************************//
PionerRobotInterface::PionerRobotInterface(PioneerRobotAPI* robotAPI, int state, float x, float y, float th)
{
	this->robotAPI->setRobot(robotAPI);
	this->state = state;
	this->position = new Pose(x, y, th);
}
//*******************************************************************************//
void PionerRobotInterface::setPose(Pose* position)
{
	this->position->setPose(position->getX(), position->getY(), position->getTh());
}
//*******************************************************************************//
Pose PionerRobotInterface::getPose()
{
	return *position;
}
//*******************************************************************************//
void PionerRobotInterface::forward(float speed)
{
	state = 1;
	this->robotAPI->moveRobot(speed);
}
//*******************************************************************************//
void PionerRobotInterface::backward(float speed)
{
	state = 1;
	robotAPI->moveRobot(-speed);
}
//*******************************************************************************//
void PionerRobotInterface::turnLeft()
{
	this->robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
}
//*******************************************************************************//
void PionerRobotInterface::turnRight()
{
	this->robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
}
//*******************************************************************************//
void PionerRobotInterface::stopTurn()
{
	this->robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::forward);
}
//*******************************************************************************//
void PionerRobotInterface::print()
{
	cout << "Position:" << position->getX() << " " << position->getY() << endl;
	cout << "State: " << state << endl;
}
//*******************************************************************************//
void PionerRobotInterface::stopMove()
{
	state = 0;
	this->robotAPI->stopRobot();
}
//*******************************************************************************//
void PionerRobotInterface::updateRobot()
{
	this->position->setPose(this->robotAPI->getX(), this->robotAPI->getY(), this->robotAPI->getTh());
}
//*******************************************************************************//
PionerRobotInterface::~PionerRobotInterface()
{
	delete this->robotAPI;
	delete this->position;
}