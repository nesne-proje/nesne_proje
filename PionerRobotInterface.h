/**
 * @file PionerRobotInterface.h
 * @Author Ezgi Ozdikyar - 152120171104
 * @date January, 2021
 * @brief This class is interface of Pioner Robot.
 */
#pragma once
#include "PioneerRobotAPI.h"
#include "Pose.h"
#include "Record.h"
#include"RobotInterface.h"
class PionerRobotInterface :public RobotInterface
{
private:
	PioneerRobotAPI* robotAPI;
public:
	PionerRobotInterface();
	/**
	* @brief: Constructor to set initialize values.
	* @param: PioneerRobotAPI robot object.
	*/
	PionerRobotInterface(PioneerRobotAPI*);
	/**
	* @brief: Constructor2 to set initialize values.
	* @param: PioneerRobotAPI obj,int state value, float x y and th values.
	*/
	PionerRobotInterface(PioneerRobotAPI*, int, float, float, float);
	/**
	* @brief: Destructor.
	* @param: PioneerRobotAPI robot object.
	*/
	 ~PionerRobotInterface();
	/**
	* @brief: This function sets position.
	* @param: Pose object.
	*/
	 void setPose(Pose*);
	/**
	* @brief: This function returs position.
	*/
	 Pose getPose();
	/**
	* @brief: Moves the robot forward.
	* @param: float speed --> moving speed.
	*/
	void forward(float);
	/**
	* @brief: Moves the robot backward.
	* @param: float speed --> moving speed.
	*/
	void backward(float);
	/**
	* @brief: Rotates the robot to the left.
	*/
	void turnLeft();
	/**
	* @brief: Rotates the robot to the right.
	*/
	void turnRight();
	/**
	* @brief: This function stops the robot from turning.
	*/
	void stopTurn();
	/**
	* @brief: This functions prints position and state values.
	*/
	void print();
	/**
	* @brief: This function stops the moving robot.
	*/
	void stopMove();
	/** This function is automatically called every one seconds.
	*/
	 void updateRobot();
};

