#include "RobotControl.h"
int RobotControl::numberOfPaths = -1;

RobotControl::RobotControl()
{
	this->state = 0;
	this->isCodeTrue = 0;
	this->record = new Record();
	this->path = new Path();
	this->robotOperator = new RobotOperator();
	record->setFileName("RecordFile.txt");
	record->openFile();
}
//*******************************************************************************//
RobotControl::RobotControl(int state,float x,float y,float th)
{
	this->state = state;
	this->isCodeTrue = 0;
	this->record = new Record();
	this->path = new Path();
	this->robotOperator = new RobotOperator();
	record->setFileName("RecordFile.txt");
	record->openFile();
}
//*******************************************************************************//
void RobotControl::setPose(Pose* position)
{
	updateRobot();
	if (isCodeTrue == 1)
		robotInterface->setPose(position);
	else return;
}
//*******************************************************************************//
Pose RobotControl::getPose()
{
	updateRobot();
	if (isCodeTrue == 1)
		return robotInterface->getPose();
}
//*******************************************************************************//
void RobotControl::forward(float speed)
{
	if (isCodeTrue == 1)
	{
		state = 1;
		robotInterface->forward(speed);
		updateRobot();
	}	
	else return;
}
//*******************************************************************************//
void RobotControl::backward(float speed)
{
	if (isCodeTrue == 1)
	{
		state = 1;
		robotInterface->backward(speed);
		updateRobot();
	}
	else return;
}
//*******************************************************************************//
void RobotControl::turnLeft()
{
	if (isCodeTrue == 1)
	{
		robotInterface->turnLeft();
		updateRobot();
	}		
	else return;
}
//*******************************************************************************//
void RobotControl::turnRight()
{	
	if (isCodeTrue == 1)
	{
		robotInterface->turnRight();
		updateRobot();
	}	
	else return;
}
//*******************************************************************************//
void RobotControl::stopTurn()
{	
	if (isCodeTrue == 1)
	{
		robotInterface->stopTurn();
		updateRobot();
	}		
	else return;
}
//*******************************************************************************//
void RobotControl::print()
{
	if (isCodeTrue == 1)
	{
		cout << "Position:" << robotInterface->getPose().getX() << " " << robotInterface->getPose().getY() << endl;
		cout << "State: " << state << endl;
	}
	else return;
}
//*******************************************************************************//
void RobotControl::stopMove()
{	
	if (isCodeTrue == 1)
	{
		state = 0;
		robotInterface->stopMove();
		updateRobot();
	}
	else return;
}
//*******************************************************************************//
void RobotControl::updateRobot()
{
	this->robotInterface->updateRobot();
	addToPath();
	recordPathToFile();
}
bool RobotControl::addToPath()
{
	if (isCodeTrue == 1)
	{
		path->addPose(robotInterface->getPose());
		numberOfPaths++;
		return true;
	}
	return false;
}
bool RobotControl::clearPath()
{
	if (isCodeTrue == 1)
	{
		bool flag = 0;
		for (int i = 0; i < path->getNumber(); i++)
		{
			flag += path->removePose(i);
		}
		return (flag == path->getNumber());
	}
	return false;
}
bool RobotControl::recordPathToFile()
{
	if (isCodeTrue == 1)
	{
		record->writeLine("[x] ");
		record->writeLine(to_string(path->getPose(numberOfPaths).getX()));
		record->writeLine("[y] ");
		record->writeLine(to_string(path->getPose(numberOfPaths).getY()));
		record->writeLine("[Angle] ");
		record->writeLine(to_string(path->getPose(numberOfPaths).getTh()));
		record->writeLine("\n");
		return true;
	}
	return false;
}
bool RobotControl::openAccess(int code)
{
	if (robotOperator->checkAccessCode(code))
	{
		isCodeTrue = 1;
		return true;
	}
	return false;	
}

bool RobotControl::closeAccess(int code)
{
	if (robotOperator->checkAccessCode(code))
	{
		isCodeTrue = 0;
		return true;
	}
	return false;
}

void RobotControl::setRobotOperator(RobotOperator* roperator)
{
	this->robotOperator = roperator;
}

void RobotControl::setRobotInterface(RobotInterface* ri)
{
	robotInterface = ri;
}

//*******************************************************************************//
RobotControl::~RobotControl()
{
	delete this->robotInterface;
	delete this->robotOperator;
	delete this->record;
	delete this->path;	
}