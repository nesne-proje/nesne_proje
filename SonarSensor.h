#pragma once
/**
* @file: SonerSensor.h
* @author: Mine ÇAKIR
* @date: 9 OCAK 2021
*/
#include "PioneerRobotAPI.h"
#include"RangeSensor.h"
class SonarSensor: public RangeSensor
{
public:
	/*
	* @brief: Constructor to set initialize values.
	* @param: PioneerRobotAPI robot object.
	*/
	SonarSensor(PioneerRobotAPI*);
	SonarSensor();
	/*
	* @brief: Destructor.
	* @param: PioneerRobotAPI robot object.
	*/
	~SonarSensor();
	/*
	* @brief: This function uploads the robot's current sensor distance values ​​to the ranges array.
	* @param: float array.
	*/
	void updateSensor(float[]);
	/*
	* @brief: This function returns the distance information of the sensor with the Index in the parameter.
	* @param: int index value.
	*/
	float getRange(int);
	/*
	* @brief: This function returns the maximum distance value.
	* The index of data with this distance returns the variable in parentheses.
	* @param: int index value.
	*/
	float getMax(int&);
	/*
	* @brief: This function returns the minimum distance value.
	* The index of data with this distance returns the variable in parentheses.
	* @param: int index value.
	*/
	float getMin(int&);
	/*
	* @brief: This function returns the angle value in the given index.
	* @param: int index value.
	*/
	float getAngle(int index);
	/*
	* @brief: This function returns the distance information of the sensor in the given index.
	* @param: int index value.
	*/
	float operator[](int) ;
	float getClosestRange(float startAngle, float endAngle, float& angle);
};

