#include"Pose.h"
using namespace std;
int main()
{
	Pose pose1(1, 2, 50);
	Pose pose2(3, 5, 54);
	if (pose1 == pose2) // Controls if two objects are equal.
		cout << "Pose 1 and Pose 2 are equal." << endl;
	else
		cout << "Pose 1 and Pose 2 are not equal." << endl;
	if (pose1 < pose2) //Controls if pose 1 is less then pose 2.
		cout << endl << "Pose 1 is less then Pose 2. " << endl;
	else
		cout << endl << "Pose 1 is greater then Pose 2. " << endl;
	
	Pose pose3 = pose1 + pose2; // Sums up pose 1 and pose 2 and assings it to new object.
	Pose pose4 = pose2 - pose1; //Subtracts pose 1 from pose 2 and assings it to new object.
	cout << endl << "--------Pose 1--------" << endl << endl;
	pose1.print();
	cout << endl << "--------Pose 2--------" << endl << endl;
	pose2.print();
	cout << endl << "--------Pose 3--------" << endl << endl;
	pose3.print();
	cout << endl << "--------Pose 4--------" << endl << endl;
	pose4.print();

	Pose pose5;
	pose5.setX(12); pose5.setY(4); pose5.setTh(90); //Assigns values using set functions.
	Pose pose6(9, 3, 85);
	cout << endl << "--------Pose 5--------" << endl << endl;
	pose5.print();
	cout << endl << "--------Pose 6--------" << endl << endl;
	pose6.print();
	cout << endl << "Distance between Pose 5 and Pose 6 : " 
		<< pose5.findDistanceTo(pose6) << endl; // Calculates distance between pose 5 and pose 6.
	cout << endl << "Angle between Pose 5 and Pose 6 : " 
		<< pose5.findAngelTo(pose5) << endl; // Calculates angle between pose 5 and pose 6.

	Pose pose7 = pose6.getPose(); //Assings pose 6 to pose 7 using getPose function.
	cout << endl << "--------Pose 7--------" << endl << endl;
	pose7.print();
	pose7.setPose(10, 15, 50); //Changes pose 7's values using setPose function.
	cout << endl << "--------Pose 7--------" << endl << endl;
	pose7.print();
		
	pose6 += pose7; //Sums up pose 6 and pose 7 and assigns it to pose 6.
	cout << endl << "--------Pose 6--------" << endl << endl;
	pose6.print();
	cout << endl << "--------Pose 7--------" << endl << endl;
	cout << "x: " << pose7.getX() << " y: " << pose7.getY() 
		<< " theta: " << pose7.getTh()<<endl; //Prints values using get functions.

	Pose pose8(10, 20, 80);
	Pose pose9(20, 40, 10);
	pose8 -= pose9; //Subtracts pose 9 from pose 8 and assigns it to pose 8.
	cout << endl << "--------Pose 8--------" << endl << endl;
	pose8.print();
	cout << endl << "--------Pose 9--------" << endl << endl;
	pose9.print();
	cout << endl << endl;
	system("pause");
}