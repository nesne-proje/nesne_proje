#include "SonarSensorMenu.h"
#include <string>
using namespace std;
void SonarSensorMenu::Menu(PioneerRobotAPI* robot)
{
	int index = 0;
	float sensorArray[16];
	SonarSensor* sRobot;
	sRobot = new SonarSensor(robot);
	string choice;
	do
	{
		cout << endl << "Sonar Sensor Menu" << endl << "1. Range" << endl << "2. Maximum" <<
			endl << "3. Minimum" << endl << "4. Angle" <<
			endl << "5. Quit" << endl;
		cout << "Choose one : ";
		getline(cin, choice);
		if (choice == "1")
		{
			cout << endl << "<Range>" << endl << "Calculating range..." << endl;
			try
			{
				cout << "Enter index: ";
				cin >> index;
				if (index >= 16)throw exception("You must enter index between 0-16");
				sRobot->updateSensor(sensorArray);
				cout << sRobot->getRange(index);
				cin.ignore();
			}
			catch (const std::exception& e)
			{
				cout << e.what() << endl;
			}
		}
		else if (choice =="2")
		{
			cout << endl << "<Maximum>" << endl << "Calculating maximum..." << endl;
			try
			{
				cout << "Enter index: ";
				cin >> index;
				if (index >= 16)throw exception("You must enter index between 0-16");
				sRobot->updateSensor(sensorArray);
				cout << sRobot->getMax(index);
				cin.ignore();
			}
			catch (const std::exception& e)
			{
				cout << e.what() << endl;
			}
		}
		else if (choice == "3")
		{
			cout << endl << "<Minimum>" << endl << "Calculating minimum..." << endl;
			try
			{
				cout << "Enter index: ";
				cin >> index;
				if (index >= 16)throw exception("You must enter index between 0-16");
				sRobot->updateSensor(sensorArray);
				cout << sRobot->getMin(index);
				cin.ignore();
			}
			catch (const std::exception& e)
			{
				cout << e.what() << endl;
			}
		}
		else if (choice == "4")
		{
			cout << endl << "<Angle>" << endl << "Calculating angle..." << endl;
			try
			{
				cout << "Enter index: ";
				cin >> index;
				if (index >= 16)throw exception("You must enter index between 0-16");
				sRobot->updateSensor(sensorArray);
				cout << sRobot->getAngle(index);
				cin.ignore();
			}
			catch (const std::exception& e)
			{
				cout << e.what() << endl;
			}
		}
		else if (choice =="5")
			break;
		else
			cout << endl << choice << " is not an option!" << endl << endl;
		cout << endl;
	} while (1);
}
