/**
 * @file Encryption.h
 * @Author Ahmet Halim UZUN
 * @date January, 2021
 * @brief It is a class to be used in authorizing the operator who will command the robot.
 */

#pragma once
class Encryption
{
public:
	/**
	* @brief: This functions encrypts the given number.
	* @param: int num - A number to encrypt.
	* @return: int encrypted - Encrypted number.
	*/
	int encrypt(int);
	/**
	* @brief: This functions decrypts the given number.
	* @param: int num - A number to decrypt.
	* @return: int decrypted - Decrypted number.
	*/
	int decrypt(int);
};