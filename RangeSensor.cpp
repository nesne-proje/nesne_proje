#include "RangeSensor.h"

RangeSensor::RangeSensor()
{
	robotAPI = new PioneerRobotAPI();
}

RangeSensor::RangeSensor(PioneerRobotAPI* r)
{
	robotAPI = r;
}

RangeSensor::~RangeSensor()
{
	delete robotAPI;
}
