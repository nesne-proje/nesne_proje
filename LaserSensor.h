/**
 * @file LaserSensor.h
 * @Author Omer Faruk LALE
 * @date January, 2021
 * @brief Provides data capture and management for laser distance sensor.
 */
#pragma once
#include "PioneerRobotAPI.h"
#include"RangeSensor.h"
class LaserSensor:public RangeSensor {
public:
	/**
	* @brief: Default constructor.
	*/
	LaserSensor();
	/**
	* @brief: Parameterized constructor.
	* @param: PioneerRobotAPI* robotAPI - object of PioneerRobotAPI
	*/
	LaserSensor(PioneerRobotAPI*);
	/**
	* @brief: Destructor.
	*/
	~LaserSensor();
	/**
	* @brief: This function returns distance information of the sensor using index parameter.
	* @param: int index - index
	* @return: float - Distance information.
	*/
	float getRange(int index);
	/**
	* @brief: This functions uploads the robot's current sensor distance values to the ranges array.
	* @param: float [] ranges - an array to store ranges
	*/
	void updateSensor(float ranges[]);
	/**
	* @brief: This functions returns the maximum of the distance values.
	* @param: int index& - index
	* @return: float max - The maximum distance value.
	*/
	float getMax(int& index);
	/**
	* @brief: This functions returns the minimum of the distance values.
	* @param: int index& - index
	* @return: float min - The minimum distance value.
	*/
	float getMin(int& index);
	/**
	* @brief: This functions returns the sensor value given the index.
	* @param: int i - index
	* @return: float - Sensor value.
	*/
	float operator[](int i);
	/**
	* @brief: This functions returns the angle value of the sensor whose index is given.
	* @param: int index - index
	* @return: float index - Angle value.
	*/
	float getAngle(int index);
	/**
	* @brief: This function returns the angle of the smallest of the distance between startAngle and endAngle angles on angle and the distance with return.
	* @param: float startAngle - First angle.
	* @param: float endAngle - Second angle.
	* @param: float angle& - Angle between startAngle and endAngle.
	* @return: float distance - Distance between startAngle and endAngle.
	*/
	float getClosestRange(float startAngle, float endAngle, float& angle);
};

