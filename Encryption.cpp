﻿#include <iostream>
#include "Encryption.h"
using namespace std;
int Encryption::encrypt(int num)
{
    int digits[4];

    for (int i = 3; i >= 0; i--) 
    {
        digits[i] = ((num % 10) + 7) % 10;
        num /= 10;
    }
 
    int encrypted = 0;
    int tmp1 = digits[0];
    int tmp2 = digits[1];
    digits[0] = digits[2];
    digits[2] = tmp1;
    digits[1] = digits[3];
    digits[3] = tmp2;
  
    for (int i = 0; i < 4; i++) 
    {
        encrypted += digits[i] * pow(10, 3 - i);
    }
    return encrypted;
}

int Encryption::decrypt(int num)
{
    int digits[4];

    for (int i = 3; i >= 0; i--) 
    {
        digits[i] = num % 10;
        num /= 10;
    }   

    int tmp1 = digits[0];
    int tmp2 = digits[1];
    digits[0] = digits[2];
    digits[2] = tmp1;
    digits[1] = digits[3];
    digits[3] = tmp2;
    int decrypted = 0;
  
    for (int i = 0; i < 4; i++) {
        digits[i] += 3;
        decrypted += digits[i] * pow(10, 3 - i);
    }
    return decrypted;
}