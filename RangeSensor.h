/**
* @file: RangeSensor.h
* @author: Ezgi Ozdikyar
* @date: January, 2021
* @brief: Interface of range classes.
*/
#pragma once
#include"PioneerRobotAPI.h"
class RangeSensor
{
protected:
	float ranges[1000];
	PioneerRobotAPI* robotAPI;
public:
	RangeSensor();
	/**
	* @brief: Parameterized constructor.
	* @param: PioneerRobotAPI* robotAPI - object of PioneerRobotAPI
	*/
	RangeSensor(PioneerRobotAPI*);
	/**
	* @brief: Destructor.
	*/
	virtual ~RangeSensor();
	/**
	* @brief: This function returns distance information of the sensor using index parameter.
	* @param: int index - index
	* @return: float - Distance information.
	*/
	virtual float getRange(int index)=0;
	/**
	* @brief: This functions uploads the robot's current sensor distance values to the ranges array.
	* @param: float [] ranges - an array to store ranges
	*/
	virtual void updateSensor(float ranges[])=0;
	/**
	* @brief: This functions returns the maximum of the distance values.
	* @param: int index& - index
	* @return: float max - The maximum distance value.
	*/
	virtual float getMax(int& index)=0;
	/**
	* @brief: This functions returns the minimum of the distance values.
	* @param: int index& - index
	* @return: float min - The minimum distance value.
	*/
	virtual float getMin(int& index)=0;
	/**
	* @brief: This functions returns the sensor value given the index.
	* @param: int i - index
	* @return: float - Sensor value.
	*/
	virtual float operator[](int i)=0;
	/**
	* @brief: This functions returns the angle value of the sensor whose index is given.
	* @param: int index - index
	* @return: float index - Angle value.
	*/
	virtual float getAngle(int index)=0;
	/**
	* @brief: This function returns the angle of the smallest of the distance between startAngle and endAngle angles on angle and the distance with return.
	* @param: float startAngle - First angle.
	* @param: float endAngle - Second angle.
	* @param: float angle& - Angle between startAngle and endAngle.
	* @return: float distance - Distance between startAngle and endAngle.
	*/
	virtual float getClosestRange(float startAngle, float endAngle, float& angle)=0;
};

