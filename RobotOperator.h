/**
 * @file RobotOperator.h
 * @Author Ahmet Halim UZUN
 * @date January, 2021
 * @brief It is a class to be used in authorizing the operator who will command the robot.
 */

#pragma once
#include"Encryption.h"
#include<iostream>
#include<string>
using namespace std;
class RobotOperator
{
private:
	Encryption enc;
	string name;
	string surname;
	unsigned int accessCode;
	bool accessState;
public:
	RobotOperator();
	/**
	* @brief: Parameterized constructor.
	* @param string name - Name of robot operator.
	* @param: string surname - Surname of robot operator.
	* @param: unsigned int num - Access code.
	*/
	RobotOperator(string, string, unsigned int);
	/**
	* @brief: The 4 digit code is encrypted using the function of the Encryption class.
	* @param: int num - Number to encrypt.
	* @return: int - Encrypted number.
	*/
	int encryptCode(int);
	/**
	* @brief: The 4 digit code is encrypted using the function of the Encryption class.
	* @param: int num - Number to decrypt.
	* @return: int - Decrypted number.
	*/
	int decryptCode(int);
	/**
	* @brief: This functions checks whether the code entered is the same as the accessCode, which is kept encrypted.
	* @param: int num - Entered number.
	* @return: bool - true or false
	*/
	bool checkAccessCode(int);
	/**
	* @brief: This functions displays the name, surname and access status of the operator.
	*/
	void print();
	void operator=(RobotOperator& r);
};

